/*
    ChibiOS - Copyright (C) 2006..2015 Giovanni Di Sirio

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

#include "ch.h"
#include "hal.h"
#include "chprintf.h"
#include "mpu6050.h"
#include <math.h>

#define RAD2DEG 54.2958


typedef struct{
  int16_t ax;
  int16_t ay;
  int16_t az;
  int16_t gx;
  int16_t gy;
  int16_t gz;
  float gyro_sens;
  float accel_sens;
}IMU;
/*
 * This is a periodic thread that does absolutely nothing except flashing
 * a LED.
 */
static const I2CConfig i2cfg = {
    OPMODE_I2C,
    100000,
    STD_DUTY_CYCLE
};

static THD_WORKING_AREA(waThread1, 128);
static THD_WORKING_AREA(ic2_1,1024);

static THD_FUNCTION(Thread1, arg) {

  (void)arg;
  chRegSetThreadName("blinker");
  while (true) {
    palSetPad(GPIOD, GPIOD_LED3);       /* Orange.  */
    chThdSleepMilliseconds(500);
    palClearPad(GPIOD, GPIOD_LED3);     /* Orange.  */
    chThdSleepMilliseconds(500);
  }
}

static THD_FUNCTION(Thread2, arg){
  (void)arg;
  BaseSequentialStream* chp = (BaseSequentialStream*) &SD2;
  IMU mpu;
  chRegSetThreadName("IC2");

  i2cStart(&I2CD1,&i2cfg);

  chThdSleepMilliseconds(100);
  MPU6050(MPU6050_ADDRESS_AD0_LOW);
  MPUinitialize();
  chThdSleepMilliseconds(10);

  mpu.ax=0;
  mpu.gx=0;
  mpu.ay=0;
  mpu.gy=0;
  mpu.az=0;
  mpu.gz=0;
  mpu.gyro_sens=131;
  mpu.accel_sens=16384;

  float dt=0.01;

  double ax=0.0;
  double ay=0.0;
  double az=0.0;
  double ax_n=0.0;
  double ay_n=0.0;
  double az_n=0.0;

  double gx=0.0;
  double gy=0.0;
  double gz=0.0;

  double R=0.0;

  double SUM_AXZ=0.0;
  double SUM_AYZ=0.0;
  double SUM_AXY=0.0;

  double Axy=0.0;
  double Ayz=0.0;
  double Axz=0.0;

  double Axy_r=0.0;
  double Ayz_r=0.0;
  double Axz_r=0.0;

// Offset for Gyroscopes
  for (int i=0;i<100;i++){
  chThdSleepMilliseconds(10);
  MPUgetMotion6(&mpu.ax,&mpu.ay,&mpu.az,&mpu.gx,&mpu.gy,&mpu.gz);

  ax=(double)mpu.ax/mpu.accel_sens;
  ay=(double)mpu.ay/mpu.accel_sens;
  az=(double)mpu.az/mpu.accel_sens;
  gx=(double)mpu.gx/mpu.gyro_sens;
  gy=(double)mpu.gy/mpu.gyro_sens;
  gz=(double)mpu.gz/mpu.gyro_sens;
  R=sqrt(ax*ax+ay*ay+az*az);
  ax_n=ax/R;
  ay_n=ay/R;
  az_n=az/R;
  SUM_AYZ+=RAD2DEG*atan2(ay_n,az_n);
  SUM_AXZ+=RAD2DEG*atan2(ax_n,az_n);
  SUM_AXY+=RAD2DEG*atan2(ay_n,ax_n);
  }
  Axy_r=SUM_AXY/100.0;
  Axz_r=SUM_AXZ/100.0;
  Ayz_r=SUM_AYZ/100.0;

  chprintf(chp,"Initialization done");

  while(1){
  chThdSleepMilliseconds(10);
  MPUgetMotion6(&mpu.ax,&mpu.ay,&mpu.az,&mpu.gx,&mpu.gy,&mpu.gz);

  ax=(double)mpu.ax/mpu.accel_sens;
  ay=(double)mpu.ay/mpu.accel_sens;
  az=(double)mpu.az/mpu.accel_sens;
  gx=(double)mpu.gx/mpu.gyro_sens;
  gy=(double)mpu.gy/mpu.gyro_sens;
  gz=(double)mpu.gz/mpu.gyro_sens;
  R=sqrt(ax*ax+ay*ay+az*az);

  ax_n=ax/R;
  ay_n=ay/R;
  az_n=az/R;
  Ayz=RAD2DEG*atan2(ay_n,az_n);
  Axz=RAD2DEG*atan2(ax_n,az_n);
  Axy=RAD2DEG*atan2(-ay_n,-ax_n);
  if(ay_n <=0.1 || ax_n <=0.1)
    Axy_r=Axy_r-gz*dt;
  else
  Axy_r=0.98*(Axy_r-gz*dt)+0.02*Ayz;

  Ayz_r=0.98*(Ayz_r+gx*dt)+0.02*Ayz;
  Axz_r=0.98*(Axz_r-gy*dt)+0.02*Axz;
  chprintf(chp,"Axy_r: %D Ayz_r: %D Axz_r: %D \r\n",(int)Axy_r,(int)Ayz_r,(int)Axz_r);
  }

}

/*
 * Application entry point.;
 */
int main(void) {

  /*
   * System initializations.
   * - HAL initialization, this also initializes the configured device drivers
   *   and performs /the board-specific initializations.
   * - Kernel initialization, the main() function becomes a thread and the
   *   RTOS is active.
   */
  halInit();
  chSysInit();
  i2cInit();
  /*
   * Activates the serial driver 2 using the driver default configuration.
   * PA2(TX) and PA3(RX) are routed to USART2.
   */
  sdStart(&SD2, NULL);
  palSetPadMode(GPIOA, 2, PAL_MODE_ALTERNATE(7));
  palSetPadMode(GPIOA, 3, PAL_MODE_ALTERNATE(7));

  palSetPadMode(GPIOB,8,
          PAL_MODE_ALTERNATE(4) |
          PAL_STM32_OTYPE_OPENDRAIN |
          PAL_STM32_OSPEED_MID1 |
          PAL_STM32_PUDR_PULLUP);

  palSetPadMode(GPIOB, 9,
          PAL_MODE_ALTERNATE(4) |
          PAL_STM32_OTYPE_OPENDRAIN |
          PAL_STM32_OSPEED_MID1 |
          PAL_STM32_PUDR_PULLUP);
  /*
   * Creates the example thread.
   */
  chThdCreateStatic(waThread1, sizeof(waThread1), NORMALPRIO+1, Thread1, NULL);
  chThdCreateStatic(ic2_1, sizeof(ic2_1), NORMALPRIO, Thread2, NULL);
  /*
   * Normal main() thread activity, in this demo it does nothing except
   * sleeping in a loop and check the button state.
   */
  BaseSequentialStream* chp = (BaseSequentialStream*) &SD2;
  while (true) {
    if (palReadPad(GPIOA, GPIOA_BUTTON)){
    chThdSleepMilliseconds(500);
    chprintf(chp,"Button Pushed \n\r");}
    chThdSleepMilliseconds(100);
  }
}
